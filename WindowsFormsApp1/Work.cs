﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Work
    {
        public string position { get; set; }
        public float period { get; set; }

        public List<string> skills { get; set; }

        public Work()
        {
            position = "";
            period = -1;
            skills = new List<string>();
        }
    }
}
