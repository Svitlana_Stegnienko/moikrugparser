﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class JavaScript
    { 
        public static string saveDocResume()
        {
            string saveString = @"
            function saveDocResume() {
	            var popup = null;
	            if (document.getElementsByClassName(""popup_inner"")[3] == undefined) {

                    var popup = document.getElementsByClassName(""popup_inner"")[0];
                }  
                else {
		            var popup = document.getElementsByClassName(""popup_inner"")[3];
                }
	            return popup.getElementsByTagName(""a"")[2].getAttribute(""href"")
            }
            saveDocResume();";

            return saveString;
        }
    
        public static string getNextLink()
        {
            return @"$('a[class=""next_page""]')[0].getAttribute(""href"");";
        }

        public static string getSkills()
        {
            return @"document.getElementsByClassName(""skills"")[0].innerText;";
        }
     
        public static string getWorkInfo()
        {
            return @"function getWorkInfo() {
	        var info = document.getElementsByClassName(""info"");

            if (info == undefined)
            {
                return [];
            }
            var result = [];
            for (var i = 0; i < info.length; ++i)
            {
                if (info[i].getElementsByClassName(""position"")[0] == undefined)
                {
                    continue;
                }
                var dict = {};
                if (info[i].getElementsByClassName(""period"")[0] != undefined)
                {
                    dict['period'] = info[i].getElementsByClassName(""period"")[0].innerText.toString();  
                } 
	            if (info[i].getElementsByClassName(""position"")[0] != undefined)
                {
                    dict['position'] = info[i].getElementsByClassName(""position"")[0].innerText.toString(); 
                }
		        if (info[i].getElementsByClassName(""skills"")[0] != undefined)
                {
                    dict['skills'] = info[i].getElementsByClassName(""skills"")[0].innerText.toString();  
                }
		        result.push(dict);
	        }
	        return result;
        }
        getWorkInfo();";
        }

        public static string openConversationDialog()
        {
            return @" function openConversationDialog()
            {
                if (document.getElementsByClassName(""button btn-big-grey"")[0] != undefined)
                {
                    document.getElementsByClassName(""button btn-big-grey"")[0].click();
                }
        }

        openConversationDialog();
            ";
        }
        public static string capchaCheck()
        {
            return @"function capchaCheck()
            {
                if (document.getElementsByClassName(""global_popup popup_confirm_friendship_request opened"")[0] != undefined)
                {  
                   // if (document.getElementsByClassName(""recaptcha-checkbox-border"")[0] != undefined)
                  //  {
                    //    document.getElementsByClassName(""recaptcha-checkbox-border"")[0].click();
                    //    if (document.getElementsByClassName(""button btn-blue"")[1] != undefined)
                    //    {
                    //       document.getElementsByClassName(""button btn-blue"")[1].click();
                    //    }
                     
                   // }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            capchaCheck();
            ";     
        }


        public static string alreadyInFriends()
        {
            return @" function connect()
            {
                if (document.getElementsByClassName(""button btn-green add_to_friends"")[0] != undefined)
                {
                    return false;
                }
                else
                {
                    return true;
                }
        }

        connect();
            ";
        }

        public static string connectToFriend()
        {
            return @" function connect()
            {
                if (document.getElementsByClassName(""button btn-green add_to_friends"")[0] != undefined)
                {
                    document.getElementsByClassName(""button btn-green add_to_friends"")[0].click();
                }
        }

        connect();
            ";
        }

        public static string sentMessage(string text)
        {
            return @"
                function sentMassage()
                {
                    $(""textarea"").val('" + text + @"');
                    document.getElementsByClassName(""form_place"")[0].getElementsByClassName(""button btn-blue"")[0].disabled = false;
                    document.getElementsByClassName(""form_place"")[0].getElementsByClassName(""button btn-blue"")[0].click();
                }

        sentMassage();
            ";
        }

        public static string getGeneralInfo()
        {
            return @"
            function getGeneralInfo() {
            
	        var contacts = document.getElementsByClassName(""contact"");
            var info = document.getElementsByClassName(""section user_info"")[0];
            var result = [];

            var dict = {};
            if (document.getElementsByClassName(""skills"")[0] != undefined)
            {
                dict['generalSkills'] = document.getElementsByClassName(""skills"")[0].innerText.toString();
            }
            if (info.getElementsByClassName(""user_name"")[0] != undefined)
            {
                dict['name'] = info.getElementsByClassName(""user_name"")[0].innerText.toString();
            }
            if (info.getElementsByClassName(""profession"")[0] != undefined && info.getElementsByClassName(""profession"")[0].innerText.length > 0)
            {
                dict['profession'] = info.getElementsByClassName(""profession"")[0].innerText.toString();
            }
            if (info.getElementsByClassName(""profession"")[1] != undefined && info.getElementsByClassName(""profession"")[1].innerText.length > 0)
            {
                dict['professionLevel'] = info.getElementsByClassName(""profession"")[1].innerText.toString();
            }
            if (document.getElementsByClassName(""salary"")[0] != undefined && document.getElementsByClassName(""salary"")[0].getElementsByClassName(""count"")[0] != undefined)
            {
                dict['salary'] = document.getElementsByClassName(""salary"")[0].getElementsByClassName(""count"")[0].innerText.toString();
            }
            if (info.getElementsByClassName(""status"")[0] != undefined)
            {
                dict['status'] = info.getElementsByClassName(""status"")[0].innerText.toString();
            }
            if (document.getElementsByClassName(""ready_to"")[0] != undefined)
            {
                 dict['status'] =  dict['status'] + "", "" + document.getElementsByClassName(""ready_to"")[0].innerText.toString();
            }

            
            if (document.getElementsByClassName(""education_show"")[0] != undefined)
            {
                var period = [];
                var education = document.getElementsByClassName(""education_show"")[0];
                if (education.getElementsByClassName(""main"")[0] != undefined) 
                {
                    var main = education.getElementsByClassName(""main"")[0];
                    var spec = """";
                    var institution_name = """"; 
                    if (main.getElementsByClassName(""specialization"")[0]  != undefined) 
                    {
                        spec =  main.getElementsByClassName(""specialization"")[0].innerText;
                    }
                    if (main.getElementsByClassName(""institution_name"")[0] != undefined) 
                    {
                        institution_name =  main.getElementsByClassName(""institution_name"")[0].innerText;
                    }
                     dict['instituteName'] = spec + "", "" + institution_name;
                }
                if (education.getElementsByClassName(""info"")[0] != undefined)
                {
                    var info = education.getElementsByClassName(""info"")[0];
                    
                    if (info.getElementsByClassName(""specialization"")[0]  != undefined) 
                    {
                        dict['faculty'] =  info.getElementsByClassName(""specialization"")[0].innerText;
                        if (info.getElementsByClassName(""description"")[0] != undefined) 
                        {
                            dict['faculty'] = dict['faculty'] + "", "" + info.getElementsByClassName(""description"")[0].innerText;
                        }
                    }
                    if (info.getElementsByClassName(""period"")[0] != undefined) 
                    {
                        period.push(info.getElementsByClassName(""period"")[0].innerText);
                    }
                }
                if (education.getElementsByClassName(""info"")[1] != undefined)
                {
                    var info = education.getElementsByClassName(""info"")[1];
                    
                    if (info.getElementsByClassName(""specialization"")[0]  != undefined) 
                    {
                        dict['faculty'] = dict['faculty'] + "", "" + info.getElementsByClassName(""specialization"")[0].innerText;

                        if (info.getElementsByClassName(""description"")[0] != undefined) 
                        {
                            dict['faculty'] = dict['faculty'] + "", "" + info.getElementsByClassName(""description"")[0].innerText;
                        }
                    }
                    if (info.getElementsByClassName(""period"")[0] != undefined) 
                    {
                        period.push(info.getElementsByClassName(""period"")[0].innerText);
                    }
                }
                if (period.length > 0)
                {
                	dict['educationPeriod'] =   period;
                }
                
            }
 
            for (var i = 0; i < contacts.length; ++i)
            {
                if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""Телефон: "")
                {
                    dict['phone'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();
                } 
		        if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""Skype: "")
		        {
                    dict['skype'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();		       
                }
		        if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""Другой: "")
		        {
                    dict['other'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();   
                }
		        if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""Twitter: "")
		        {
                    dict['twitter'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();
                }
		        if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""Почта: "")
		        {
                    dict['email'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();
                }
		        if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""GitHub: "")
		        {
                    dict['gitHub'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();
                }
		        if (contacts[i].getElementsByClassName(""type"")[0].innerText == ""Telegram: "")
		        {
                    dict['telegram'] = contacts[i].getElementsByClassName(""value"")[0].innerText.toString();
                }
            }
            result.push(dict);

            return result;
        }

        getGeneralInfo();
        ";
        }
       // document.getElementsByClassName(""text"")[0].value = ""ekaterina.golubova @ukr.net"";
       // document.getElementsByClassName(""text"")[1].value = ""popkorn01"";
        public static string login()
        {
            string loginText = @"
            function login()
            {
                document.getElementsByClassName(""text"")[0].value = ""ekaterina.golubova@ukr.net"";
                document.getElementsByClassName(""text"")[1].value = ""popkorn01"";
                document.getElementsByClassName(""btn-blue"")[0].click();
            }
            
            login();
            ";
            return loginText;
        }

        public static string getUserLinks()
        {
            string links = @"

            function getUserLinks() 
            {
                var users = $('div[class=""username""]').toArray();

                var list = [];
                for (var i = 0; i < users.length; ++i)
                {
                    var link = users[i].getElementsByTagName(""a"")[0].getAttribute(""href"").toString();
                    if (link != """")
                    {
                        list.push(link);
                    }
                }
         
                return list;
            }

             getUserLinks();
            ";

            return links;
        }
    }
}
