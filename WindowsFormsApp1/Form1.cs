﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Timers;
using System.Threading.Tasks;
using System.Net;
//using Json.Net;
using System.IO;
using Newtonsoft.Json;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form//, ILifeSpanHandler
    {

        public int connectToFriendTimerSec;
        public bool waitForConnectToFriend;
        public List<object> links;
        public List<object> fullLinks;
        public string nextListLink;
        public int randomWatingTime;
        public User currentUser;
        bool isPause;

        public Form1()
        {
            waitForConnectToFriend = false;
            connectToFriendTimerSec = 15;
            isPause = false;
            fullLinks = new List<object>();

            InitializeComponent();
        }

        ChromiumWebBrowser chrome;

        public object callback {
            get; private set;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            textBox2.Text = @"F:\resume_moikrug\";
            chrome = new ChromiumWebBrowser("https://moikrug.ru/users/sign_in");
            //chrome = new ChromiumWebBrowser("https://moikrug.ru/resumes?q=%D0%95%D0%BA%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%BD%D0%B0+%D0%9C%D0%B0%D0%BB%D0%B5%D1%86%D0%BA%D0%B0%D1%8F&currency=rur");

            this.pContainer.Controls.Add(chrome);
            chrome.Dock = DockStyle.Fill;
            this.chrome.ExecuteScriptAsyncWhenPageLoaded(JavaScript.login(), true);
        }

        private void button1_Click(object sender, EventArgs e) 
        {
            button2.Visible = false;
            if (button1.Text == @"Start")
            {
                isPause = false;
                handleUserList();
                
                button1.Text = @"Pause";
               
            }
            else if (button1.Text == @"Pause")
            {       
                button1.Text = @"Continue";
                isPause = true;
            }
            else if (button1.Text == @"Continue")
            {
                
               button1.Text = @"Pause";
               
                isPause = false;
                handleNextUser();
            }
        }

        private async void handleUserList()
        {
            Task<CefSharp.JavascriptResponse> nextLink = this.chrome.EvaluateScriptAsync(JavaScript.getNextLink());
            nextLink.Wait();
            if (nextLink.Result.Result != null)
            {
                nextListLink = nextLink.Result.Result.ToString();
            }
            else
            {
                nextListLink = null;
            }

            Task<CefSharp.JavascriptResponse> userLinks = this.chrome.EvaluateScriptAsync(JavaScript.getUserLinks());
            userLinks.Wait();
            if (userLinks.Result.Result != null)
            {
             
                links = (List <object>)userLinks.Result.Result;
                
                handleNextUser();
            }
            else
            {
                stopWork();
            }

        }

        public void handleNextUser()
        {
            if (isPause)
            {
                return;
            }
            if (links != null && links.Count > 0)
            {
                dynamic linkObj = links[0];
                links.RemoveAt(0);
                string link = linkObj;
                if (link.Length != 0 && fullLinks.Contains(link) != true)
                {
                    fullLinks.Add(link);
                    string fullLink = "https://moikrug.ru" + link;
                    chrome.Load(fullLink);
                    System.Threading.Timer timer = null;
                    Task<CefSharp.JavascriptResponse> contactsScript = null;
                    do
                    {
                        contactsScript = this.chrome.EvaluateScriptAsync(JavaScript.getGeneralInfo());

                        contactsScript.Wait();
                        
                    } while (contactsScript.Result.Result == null);//*/


                   // timer = new System.Threading.Timer(async (objTimer) =>
                  //  {
                        contactsScript = this.chrome.EvaluateScriptAsync(JavaScript.getGeneralInfo());

                        contactsScript.Wait();
                        GetInfo((List<object>)contactsScript.Result.Result);
                        currentUser.userID = link.Substring(1, link.Length - 1);

                        if (currentUser.mail == @"" && currentUser.skype == @"" && currentUser.telegram == @"" && currentUser.twitter == @"" && currentUser.phone == @""
                        && currentUser.github == @"")
                        {
                            connectToFriend();  
                        }
                        else
                        {
                            saveUserInfo(currentUser);
                            handleNextUser();
                         }

                    //    timer.Dispose();
                 //   }, null, 1500, System.Threading.Timeout.Infinite);
                }
                else
                {
                    handleNextUser();
                }
            }
            else
            {
                openNextList();
            }
           
        }

        private async void saveUserInfo(User user)
        {
            Task<CefSharp.JavascriptResponse> saveResumeLink = this.chrome.EvaluateScriptAsync(JavaScript.saveDocResume());
            saveResumeLink.Wait();
            if (saveResumeLink.Result.Result != null)
            {
                var link = saveResumeLink.Result.Result.ToString();
                WebClient webClient = new WebClient();

                webClient.DownloadFileAsync(new Uri(link), textBox2.Text + user.userID + @".docx");
            }

             using (StreamWriter file = File.AppendText(textBox2.Text + @"path.txt"))
            {
                JsonSerializer serializer = new JsonSerializer();
                string jsonData = JsonConvert.SerializeObject(user);
                serializer.Serialize(file, jsonData);
            }

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            connectToFriendTimerSec += 1;

            textBox1.BeginInvoke(new MethodInvoker(() =>
            {
                textBox1.Text = connectToFriendTimerSec.ToString();
            }));

            Random rnd = new Random();

            int waitSec = rnd.Next(20 - connectToFriendTimerSec , 20);

            if (waitForConnectToFriend == true && connectToFriendTimerSec > waitSec)
            {
                connectToFriend();
            }
        }

        private void stopWork()
        {
            button1.BeginInvoke(new MethodInvoker(() =>
            {
                button1.Text = @"Start";
                isPause = true;
            }));
        }

        private async void connectToFriend()
        {
            Task<CefSharp.JavascriptResponse> alreadyInFriendsScript = this.chrome.EvaluateScriptAsync(JavaScript.alreadyInFriends());
            alreadyInFriendsScript.Wait();
            bool alreadyInFriends = true;
            if (alreadyInFriendsScript.Result != null)
            {
                alreadyInFriends = (bool)alreadyInFriendsScript.Result.Result;
            }

            Random rnd = new Random();
            randomWatingTime = rnd.Next(10, 15);

            textBox1.BeginInvoke(new MethodInvoker(() =>
            {
                textBox1.Text = connectToFriendTimerSec.ToString();
            }));

            if (connectToFriendTimerSec > randomWatingTime && alreadyInFriends == false)
            {
                Task<CefSharp.JavascriptResponse> successConnect = this.chrome.EvaluateScriptAsync(JavaScript.connectToFriend());
                successConnect.Wait();
                connectToFriendTimerSec = 0;
                waitForConnectToFriend = false;
                System.Threading.Timer timer = null;
                timer = new System.Threading.Timer(async (objTimer) =>
                {
                    Task<CefSharp.JavascriptResponse> task = this.chrome.EvaluateScriptAsync(JavaScript.capchaCheck());
                    task.Wait();
                    if (task.Result.Result != null && (bool)task.Result.Result == true)
                    {
                        capchaDidAppeare();
                    }
                    else
                    {
                        writeMessage();
                    }

                    timer.Dispose();
                }, null, 1000, System.Threading.Timeout.Infinite);
                
            }
            else if (connectToFriendTimerSec < randomWatingTime && alreadyInFriends == false)
            {
                waitForConnectToFriend = true;
            }
            else if (alreadyInFriends == true)
            {
                System.Console.WriteLine("can not connect to friend");
                
                waitForConnectToFriend = false;
                handleNextUser();
            }
        }

        private bool isRussionString(string line)
        {
            int Ru = 0, En = 0;
            line = line.ToUpper();
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if ((c >= 'А') && (c <= 'Я'))
                    Ru++;
                else if ((c >= 'A') && (c <= 'Z')) En++;
            }

            return Ru > En;
        }


        private void writeMessage()
        {
            Task<CefSharp.JavascriptResponse> success = this.chrome.EvaluateScriptAsync(JavaScript.openConversationDialog());
            success.Wait();
            
            System.Threading.Timer timer = null;
            timer = new System.Threading.Timer(async (objTimer) =>
            {
                bool isRussionName = isRussionString(currentUser.firstName);
                var text = @"Hello! I am a Head of IT Recruitment at IT Hunter company. Our company specializes in worldwide Relocation/Remote of IT professionals. We work with Fortune 200 companies. Would be glad to share career opportunities with you. Detailed information and opened positions you can see on our agency website: http://ithunter.pro. Regards, Ekaterina";

                if (currentUser.firstName.Length > 2)
                {
                    text = @"Hello " + currentUser.firstName + @"! I am a Head of IT Recruitment at IT Hunter company. Our company specializes in worldwide Relocation/Remote of IT professionals. We work with Fortune 200 companies. Would be glad to share career opportunities with you. Detailed information and opened positions you can see on our agency website: http://ithunter.pro. Regards, Ekaterina.";
                    if (isRussionName)
                    {
                        text = @"Здравствуйте " + currentUser.firstName + @"! Я работаю в рекрутиноговой компании ITHunter. Наша компания специализируется на релокации и удаленной работе IT специалистов по всему миру. Мы работаем с ведущими компаниями Европы, Америки и Австралии. Будем рады поделиться с вами возможностями карьерного роста. Детальную информацию и открытые вакансии вы можете посмотреть на сайте компани: http://ithunter.pro. С уважением, Екатерина.";
                    }
                }
               
                Task < CefSharp.JavascriptResponse > task = this.chrome.EvaluateScriptAsync(JavaScript.sentMessage(text));
                task.Wait();

                handleNextUser();
                timer.Dispose();
            }, null, 500, System.Threading.Timeout.Infinite);
        }

        private void openNextList()
        {
            if (nextListLink != null)
            {
                string fullLink = "https://moikrug.ru" + nextListLink;
                chrome.Load(fullLink);

                System.Threading.Timer timer = null;
                timer = new System.Threading.Timer(async (objTimer) =>
                {
                    this.handleUserList();
                    timer.Dispose();
                }, null, 1500, System.Threading.Timeout.Infinite);
            }
            else
            {
                stopWork();
            }
        }

        private async void GetInfo(List<object> contacts)
        {
            User user = JavaScriptParser.invokeGeneralInfo(contacts);
            if (user == null || user.userID == "")
            {
                user.userID = Guid.NewGuid().ToString();
            }

            Task<CefSharp.JavascriptResponse> workInfoScript = this.chrome.EvaluateScriptAsync(JavaScript.getWorkInfo());
            workInfoScript.Wait();
            List<object> workInfo = (List<object>)workInfoScript.Result.Result;
            user.workInfo = JavaScriptParser.invokeWorkInfo(workInfo);
            if (user == null)
            {
                System.Console.WriteLine("sss");
            }
            currentUser = user;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Visible = false;
            button1.Invoke((MethodInvoker)(() =>
            {
                button1_Click(null, null);
            }));
            

        }

        private void capchaDidAppeare()
        {
            button2.BeginInvoke(new MethodInvoker(() =>
            {
                button2.Visible = true;
            }));

            button1.Invoke((MethodInvoker)(() =>
            {
                button1.Text = @"Continue";
                isPause = true;
            }));

        }

        // ---------------------------------ILifeSpanHandler---------------------------------------------------------

       /* public bool OnBeforePopup(IWebBrowser browserControl, IBrowser browser, IFrame frame, string targetUrl, string targetFrameName,
            WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo,
            IBrowserSettings browserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
        {
           //  frame.LoadUrl(targetUrl);
             newBrowser = null;
            //  return true;
            button2.BeginInvoke(new MethodInvoker(() =>
            {
                button2.Visible = true;
            }));
            
            isPause = true;
            return false;
        }

        public void OnAfterCreated(IWebBrowser browserControl, IBrowser browser)
        {
            //
        }

        public bool DoClose(IWebBrowser browserControl, IBrowser browser)
        {
            return false;
        }

        public void OnBeforeClose(IWebBrowser browserControl, IBrowser browser)
        {
            //nothing
        }*/

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void PContainer_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
