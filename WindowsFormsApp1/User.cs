﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using Json.Net;
//using Work;

namespace WindowsFormsApp1
{
    //[DataContract]
    public class User
    {

        public string userID { get; set; }

        public string fullName { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string profession { get; set; }

        public string professionLevel { get; set; }

        public string salary { get; set; }

        public string status { get; set; }

        public string instituteName { get; set; }

        public string faculty { get; set; }

        public float educationPeriod { get; set; }

        public string hasHeigherEducation { get; set; }

        public string mail { get; set; }

        public string telegram { get; set; }

        public string  phone { get; set; }

        public string twitter { get; set; }

        public string github { get; set; }

        public string skype { get; set; }

        public string other { get; set; }

        public string skills { get; set; }

        public List<Work> workInfo { get; set; }

        public User()
        {
            firstName = @"";
            lastName  = @"";
            fullName = @"";
            profession = @"";
            professionLevel = @"";
            salary = @"";
            status = @"";
            userID = @"";
            mail = @"";
            telegram = @"";
            phone = @"";
            twitter = @"";
            github = @"";
            skype = @"";
            other = @"";
            skills = @"";
            workInfo = new List<Work>();
            instituteName = @"";
            faculty = @"";
            educationPeriod = 0;
            hasHeigherEducation = @"unknown";
    }

    }
}