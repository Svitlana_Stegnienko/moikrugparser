﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class JavaScriptParser
    {
        static private float convertPeriod(string period)
        {
            var index = period.IndexOf("(");
            if (-1 != index)
            {
                int year = 0;
                int month = 0;
                var digit = "";
                for (int i = index; i < period.Length; ++i)
                {
                    var symbol = period.Substring(i, 1);
                    if (int.TryParse(symbol, out int n))
                    {
                        digit = digit + period.Substring(i, 1);
                    }
                    else if ((symbol == "г" || symbol == "л") & digit != "")
                    {
                        _ = int.TryParse(digit, out year);
                        digit = "";
                    }
                    else if (symbol == "м" & digit != "")
                    {
                        _ = int.TryParse(digit, out month);
                        digit = "";
                    }
                }

                return (float)year + (float)month / 12;
            }
            return 0;
        }
        static public List<Work> invokeWorkInfo(List<object> workInfo)
        {
            if (workInfo == null || workInfo.Count == 0)
            {
                return null;
            }

            var fullWorkList = new List<Work>();
            foreach (object obj in workInfo)
            {
                if (obj == null)
                {
                    continue;
                }
                dynamic element = obj;
                IDictionary<string, object> values = element;
                var work = new Work();
                if (values.ContainsKey("position"))
                {
                    work.position = values["position"].ToString();
                }
                if (values.ContainsKey("skills"))
                {
                    work.skills = values["skills"].ToString().Split(new string[] { " • " }, StringSplitOptions.None).ToList<string>();
                }
                if (values.ContainsKey("period"))
                {
                    var period = values["period"].ToString();

                    work.period = JavaScriptParser.convertPeriod(period);
                }

                fullWorkList.Add(work);
            }

            return JavaScriptParser.filterWorkList(fullWorkList);
        }

        private static List<Work> filterWorkList(List<Work> workList)
        {
            if (workList.Count == 0)
            {
                return workList;
            }
            
            var finalWorkList = new List<Work>();
            finalWorkList.Add(workList.First());
            float wholePeriod = workList.First().period;
            for (int i = 1; i < workList.Count; ++i)
            {
                var previousWork = finalWorkList.Last();
                var currentWork = workList[i];
                if (currentWork.position.Length == 0)
                {
                    continue;
                }
                wholePeriod += currentWork.period;
                if (previousWork.position.ToLower().Contains(currentWork.position.ToLower()) || currentWork.position.ToLower().Contains(previousWork.position.ToLower()))
                {
                    var work = new Work();
                    work.period = previousWork.period + currentWork.period;
                    work.skills = currentWork.skills.Union(previousWork.skills).ToList();
                    work.position = previousWork.position;
                    if (currentWork.position.Contains(previousWork.position))
                    {
                        work.position = currentWork.position;
                    }

                    finalWorkList.RemoveAt(finalWorkList.Count - 1);
                    finalWorkList.Add(work);
                }
                else if (wholePeriod < 9)
                {
                    finalWorkList.Add(currentWork);
                }
                else
                {
                    break;
                }
            }

            return finalWorkList;
        }

        static public User invokeGeneralInfo(List<object> contacts)
        {
            User user = new User();

            if (contacts == null || contacts.Count == 0)
            {
                return user;
            }

            dynamic element = contacts[0];
            IDictionary<string, object> values = element;

            if (values.ContainsKey("name"))
            {
                user.fullName = values["name"].ToString();
                var fullName = values["name"].ToString().Split(new string[] { " " }, StringSplitOptions.None).ToList<string>();
                if (fullName.Count == 2)
                {
                    user.firstName = fullName[0];
                    user.lastName = fullName[1];
                }
            }
            if (values.ContainsKey("profession"))
            {
                user.profession = values["profession"].ToString();
            }
            if (values.ContainsKey("professionLevel"))
            {
                user.professionLevel = values["professionLevel"].ToString();
            }
            if (values.ContainsKey("salary"))
            {
                user.salary = values["salary"].ToString();
            }
            if (values.ContainsKey("status"))
            {
                user.status = values["status"].ToString();
            }
            if (values.ContainsKey("phone"))
            {
                user.phone = values["phone"].ToString();
            }
            if (values.ContainsKey("skype"))
            {
                user.skype = values["skype"].ToString();
            }
            if (values.ContainsKey("other"))
            {
                user.other = values["other"].ToString();
            }
            if (values.ContainsKey("twitter"))
            {
                user.twitter = values["twitter"].ToString();
            }
            if (values.ContainsKey("email"))
            {
                user.mail = values["email"].ToString();
            }
            if (values.ContainsKey("gitHub"))
            {
                user.github = values["gitHub"].ToString();
            }
            if (values.ContainsKey("telegram"))
            {
                user.telegram = values["telegram"].ToString();
            }
            if (values.ContainsKey("generalSkills"))
            {
                user.skills = values["generalSkills"].ToString();
            }
            if (values.ContainsKey("instituteName"))
            {
                user.instituteName = values["instituteName"].ToString();
            }
            if (values.ContainsKey("faculty"))
            {
                user.faculty = values["faculty"].ToString();
            }
            if (values.ContainsKey("educationPeriod"))
            {
                List<object> period = (List<object>)values["educationPeriod"];

                user.educationPeriod = JavaScriptParser.convertPeriod(period[0].ToString());
                if (period.Count > 1)
                {
                    var period2 = JavaScriptParser.convertPeriod(period[1].ToString());
                    user.educationPeriod += period2;
                }
            }

            if (user.faculty == @"" && user.instituteName == @"")
            {
                user.hasHeigherEducation = @"unknown";
            }
            else if (user.educationPeriod > 4.3)
            {
                user.hasHeigherEducation = @"yes";

                if (user.faculty != "")
                {
                    var name = user.faculty.ToLower();
                    if (name.Contains("технич") || name.Contains("информ") || name.Contains("компьют") || name.Contains("инженер")
                         || name.Contains("матем") || name.Contains("физик") || name.Contains("электрон") || name.Contains("электрон") || name.Contains("програм")
                         || name.Contains("систем") || name.Contains("прикладн") || name.Contains("анализ") || name.Contains("автомати"))
                    {
                        user.hasHeigherEducation = @"yesIT";
                    }
                }
                else if (user.instituteName != "")
                {
                    var name = user.instituteName.ToLower();
                    if (name.Contains("технич") || name.Contains("информ") || name.Contains("компьют") || name.Contains("инженер")
                         || name.Contains("матем") || name.Contains("физик") || name.Contains("электрон") || name.Contains("электрон") || name.Contains("програм")
                         || name.Contains("систем") || name.Contains("прикладн") || name.Contains("анализ") || name.Contains("автомати"))
                    {
                        user.hasHeigherEducation = @"yesIT";
                    }
                }
            }
            else
            {
                user.hasHeigherEducation = @"no";
            }

            return user;
        }
    }
}
